# Class: dns
# ===========================
#
# A class to manage /etc/resolv.conf
#
# Parameters
# ----------
#
# nameservers: Array of hostname entries for /etc/resolv.conf
# localhosts: Array of additional lines for /etc/hosts
#
# Examples
# --------
#
#    class { 'dns':
#      nameservers => [ '192.168.0.1', '8.8.4.4' ],
#    }
#
# Authors
# -------
#
# Cloud2class
#
# Copyright
# ---------
#
# Copyright 2016 Bright Process Ltd
#

class dns (
  Array $nameservers = [],
  Array $localhosts = [],
) {

  if is_defined($localhosts) {
    $localhosts.each |String $hostentry| {
      file_line { "fileline-${hostentry}":
        path   => '/etc/hosts',
        ensure => present,
        line   => $hostentry,
      }
    }
  }

  if is_defined($nameservers) {
    file { '/etc/resolv.conf':
      ensure => present,
      owner => 'root',
      group => 'root',
      mode  => '0644',
      content => template('dns/resolv.conf.erb')
    }
  }
}

