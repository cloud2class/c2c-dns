# dns

# A simple demonstration puppet module

# Class: dns
# ===========================
#
# A class to manage /etc/resolv.conf
#
# Parameters
# ----------
#
# nameservers: Array of hostname entries for /etc/resolv.conf
# localhosts: Array of additional lines for /etc/hosts
#
# Examples
# --------
#
#    class { 'dns':
#      nameservers => [ '192.168.0.1', '8.8.4.4' ],
#    }

